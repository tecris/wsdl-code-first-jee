package org.terra.ws.jaxws.codefirst;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.terra.ws.jaxws.codefirst.CityService;

/**
 * Simple set of tests to demonstrate accessing the web service using a client
 * 
 */
public class ClientIT {

    private static final Logger LOGGER = Logger.getLogger(ClientIT.class.getName());

    private static final String[] CITIES = { "Athens", "Istanbul", "Bucharest", "Budapest", "Vienna", "Prague", "Berlin",
            "Amsterdam", "Rome", "Madrid" };

    /**
     * The path of the WSDL endpoint in relation to the deployed web application.
     */
    private static final String WSDL_PATH = "CityService?wsdl";

    private static URL DEPLOYMENT_URL;

    private CityService client;

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {

        // Set the deployment url
        ClientIT.DEPLOYMENT_URL = new URL("http://localhost:8080/cityService/" + WSDL_PATH);
    }

    @Before
    public void setup() {
        try {
            client = new Client(new URL(DEPLOYMENT_URL, WSDL_PATH));
        } catch (MalformedURLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testCityBreak() {
        LOGGER.info("[Client] Requesting Inspire Me City Break.");

        // Get a response from the WebService
        final String response = client.cityBreakInspiration();
        assertTrue(Arrays.asList(CITIES).contains(response));

        LOGGER.info("[WebService] " + response);
    }

    @Test
    public void testGetCity() {
        LOGGER.info("[Client] Requesting Cities starting with letter A.");

        // Get a response from the WebService
        List<String> cityList = client.getCitiesStartingWith('A');
        assertEquals(cityList.size(), 2);
        assertTrue(cityList.contains("Athens"));
        assertTrue(cityList.contains("Amsterdam"));

        LOGGER.info("[WebService] " + cityList);
    }
}
