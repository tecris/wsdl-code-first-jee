package org.terra.ws.jaxws.codefirst;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.terra.ws.jaxws.codefirst.CityService;

/**
 * Client stub to the HelloWorld JAX-WS Web Service.
 * 
 */
public class Client implements CityService {
    private CityService cityService;

    /**
     * Default constructor
     * 
     * @param url The URL to the Hello World WSDL endpoint.
     */
    public Client(final URL wsdlUrl) {
        QName serviceName = new QName("https://github.com/tecris/org.terra.ws.jaxws.codefirst", "CityService");

        Service service = Service.create(wsdlUrl, serviceName);
        cityService = service.getPort(CityService.class);
        assert (cityService != null);
    }

    /**
     * Default constructor
     * 
     * @param url The URL to the Hello World WSDL endpoint.
     * @throws MalformedURLException if the WSDL url is malformed.
     */
    public Client(final String url) throws MalformedURLException {
        this(new URL(url));
    }

    @Override
    public String cityBreakInspiration() {
        return cityService.cityBreakInspiration();
    }
    
    @Override
    public List<String> getCitiesStartingWith(char c) {
        return cityService.getCitiesStartingWith(c);
    }
}
