package org.terra.ws.jaxws.codefirst;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

/**
 * The implementation of the Web Service.
 * 
 */
@WebService(serviceName = "CityService", portName = "City", name = "City", endpointInterface = "org.terra.ws.jaxws.codefirst.CityService", targetNamespace = "https://github.com/tecris/org.terra.ws.jaxws.codefirst")
public class CityServiceImpl implements CityService {
    
    private static final String[] CITIES = {"Athens", "Istanbul", "Bucharest", "Budapest", "Vienna", "Prague", "Berlin", "Amsterdam", "Rome", "Madrid"};

    @Override
    public String cityBreakInspiration() {
        
        return CITIES[(int) (Math.random() * 10)];
    }

    @Override
    public List<String> getCitiesStartingWith(char c) {

        List<String> cityList = new ArrayList<>();
        for(String city : CITIES) {
            if(city.startsWith(String.valueOf(c))) {
                cityList.add(city);
            }
        }
        return cityList;
    }
}
