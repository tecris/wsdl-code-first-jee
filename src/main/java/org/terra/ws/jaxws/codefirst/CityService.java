package org.terra.ws.jaxws.codefirst;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * A simple JAX-WS Web Service example.
 * 
 */
@WebService(targetNamespace = "https://github.com/tecris/org.terra.ws.jaxws.codefirst")
public interface CityService {

    /**
     * Returns random city.
     * 
     * @return A random city
     */
    @WebMethod
    String cityBreakInspiration();

    /**
     * Return cities starting with given letter
     * @param c 
     * 
     * @return list of cities
     */
    @WebMethod
    List<String> getCitiesStartingWith(char c);
}
