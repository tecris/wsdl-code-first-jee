# Boilerplate JAX-WS Web Services (code first)

Stack:
 - java 8
 - wildfly 9
 - maven 3.3
 - docker 1.9

## How to run application
### With Docker
- Build docker images.
 - `# ./pre_requisites.sh`
 - Follow [instructions](https://github.com/tecris/docker/blob/v3.6/nexus/README.md) to add jboss repository (as proxy repository) to nexus
- Start wildfly container.
 - `# docker run -d --name jax-demo -p 8080:8080 -p 9990:9990 casa.docker/wildfly:9.0.2`
- Deploy 
 - `# mvn clean wildfly:deploy`
- Undeploy
 - `# mvn clean wildfly:undeploy`

### Run integration tests -  automated
Minimalistic continuous delivery
- `# mvn -Pcd clean verify`
- `# mvn -Pcd clean integration-test` - will let web(wildfly) container up and running

Use `-Dmaven.buildNumber.doCheck=false` if project contains local changes

## Test deployment - manually
 - `# ./postRequest.sh`

## Create tag with maven 
 - `$ mvn clean -Darguments="-DskipITs" release:prepare`
 - -Darguments="-DskipITs"  - skip integration tests
 
##  WSDL
- http://localhost:8080/cityService/CityService?wsdl
